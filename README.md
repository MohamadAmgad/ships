## Getting Started with the project

## Available Scripts

In the project directory, you can run:

### `npm install`

To Install node modules

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Used libraries

1. React
2. Typescript
3. Material UI
4. graphql
5. Apollo client

### Functionality

1. List of ships is displayed to the user
2. User can switch beteen list view and grid mode
3. User can filter ships by type
4. User can scroll infinitly in the list

### Future work

Due to the limited time, things to add in the future are:

1. Add some UI/UX improvement
2. Display more info about every ship
3. Tests
4. Responsive
