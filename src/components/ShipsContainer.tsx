import React, { useContext } from "react";

import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { ShipsContainerProps } from "../types/types";
import { shipsContext } from "../provider/ShipsProvider";
import { ShipsInfo } from "./ShipsInfo";
import { ImageShips } from "./ImageShips";
import "./ShipContainer.scss";

const ShipsContainer: React.FC<ShipsContainerProps> = () => {
    const [value, setValue] = React.useState("1");
    const { ships, filterShips } = useContext(shipsContext);
    const shipTypes = ["High Speed Craft", "Cargo", "Tug", "Barge"];

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };

    const handleFilter = (e: React.ChangeEvent<HTMLSelectElement>) => {
        filterShips(e.target.value);
    };

    return (
        <Box sx={{ width: "100%", typography: "body1" }}>
            <TabContext value={value}>
                <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                    <TabList
                        onChange={handleChange}
                        aria-label="lab API tabs example"
                    >
                        <Tab label="ships" value="1" />
                        <Tab label="grid view" value="2" />
                    </TabList>
                </Box>

                <div className="dropdown">
                    <select
                        onChange={(e) => {
                            handleFilter(e);
                        }}
                    >
                        <option value="Type">Select Type</option>
                        {shipTypes &&
                            shipTypes.length > 0 &&
                            shipTypes.map((type) => {
                                return (
                                    <option key={type} value={type}>
                                        {type}
                                    </option>
                                );
                            })}
                    </select>
                </div>

                <TabPanel value="1">
                    <div>
                        <ShipsInfo ships={ships} />
                    </div>
                </TabPanel>
                <TabPanel value="2">
                    <div>
                        <ImageShips ships={ships} />
                    </div>
                </TabPanel>
            </TabContext>
        </Box>
    );
};

export default ShipsContainer;
