import { useContext } from "react";
import { shipsContext } from "../provider/ShipsProvider";
import { Ship } from "../types/types";
import InfiniteScroll from "react-infinite-scroll-component";

type Props = {
    ships: Ship[];
};
export const ImageShips: React.FC<Props> = ({ ships }) => {
    const { scrollPage, pageNumber } = useContext(shipsContext);
    return (
        <>
            <InfiniteScroll
                dataLength={ships.length}
                next={scrollPage}
                hasMore={pageNumber < ships.length + 5}
                loader={<h3>Loading...</h3>}
                endMessage={<h3>That's ALL</h3>}
            >
                <div>
                    {ships &&
                        ships.length > 0 &&
                        ships.map(({ id, image }) => (
                            <div key={id}>
                                <img
                                    alt="grid mode"
                                    style={{ width: "200px", height: "200px" }}
                                    src={image}
                                />
                            </div>
                        ))}
                </div>
            </InfiniteScroll>
        </>
    );
};
