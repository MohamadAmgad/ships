import { Ship } from "../types/types";
import InfiniteScroll from "react-infinite-scroll-component";
import { useContext } from "react";
import { shipsContext } from "../provider/ShipsProvider";
import "./ShipsInfo.scss";

type Props = {
    ships: Ship[];
};
export const ShipsInfo: React.FC<Props> = ({ ships }) => {
    const { scrollPage, pageNumber } = useContext(shipsContext);
    return (
        <>
            <InfiniteScroll
                dataLength={ships.length}
                next={scrollPage}
                hasMore={pageNumber < ships.length + 5}
                loader={<h4>Loading...</h4>}
                endMessage={<h2>That's ALL</h2>}
            >
                {ships.map(({ id, name, type, year_built, image }) => (
                    <div className="container" key={id}>
                        <img alt="list view " src={image} />
                        <div className="container__name">Name: {name}</div>
                        <div className="container__type">Type: {type}</div>
                        <div className="container__year">
                            Year Build: {year_built}
                        </div>
                    </div>
                ))}
            </InfiniteScroll>
        </>
    );
};
