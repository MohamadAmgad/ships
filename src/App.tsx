import React from "react";
import ShipsContainer from "./components/ShipsContainer";

function App() {
    return (
        <div className="App">
            <ShipsContainer />
        </div>
    );
}

export default App;
