export type Ship = {
    id: number;
    name: string;
    home_port: string;
    image: string;
    type: string;
    year_built: string;
};

export type ShipsContainerProps = {
    ships?: Ship[];
};
