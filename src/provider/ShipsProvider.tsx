import { createContext, ReactElement, useEffect, useState } from "react";
import { Ship } from "../types/types";
import { gql, useQuery } from "@apollo/client";

const GET_SHIPS = gql`
    query getShips($offset: Int) {
        ships(offset: $offset, limit: 5) {
            type
            image
            name
            id
            year_built
        }
    }
`;

export type Ships = {
    ships: Ship[];
    filterShips: (type: string) => void;
    scrollPage: () => void;
    pageNumber: number;
};

export const shipsContext = createContext<Ships>({
    ships: [],
    filterShips: () => void {},
    scrollPage: () => void {},
    pageNumber: 0,
});

type ShipsProviderProps = {
    children?: ReactElement | ReactElement[];
};

const ShipsProvider = ({ children }: ShipsProviderProps): ReactElement => {
    const [currentShips, setCurrentShips] = useState<Ship[]>([]);
    const [allShips, setAllShips] = useState<Ship[]>([]);
    const [filter, setFilter] = useState("");
    const [page, setPage] = useState(0);

    const { fetchMore, data } = useQuery(GET_SHIPS, {
        variables: { offset: 0 },
        onCompleted: (data) => {
            if (!currentShips.length) {
                setCurrentShips(data.ships);
                setPage(page + 5);
            }
        },
    });

    const getMoreShips = async () => {
        const { data } = await fetchMore({
            variables: { offset: currentShips.length },
        });

        setCurrentShips((prevShips) => [...prevShips, ...data.ships]);
        setAllShips((prevShips) => [...prevShips, ...data.ships]);
        setPage(page + 5);
    };

    useEffect(() => {
        if (data) {
            if (filter) {
                handleFilter();
            } else {
                setCurrentShips(data.ships);
                setAllShips(data.ships);
            }
        }
    }, [data, filter]);

    const handleFilter = () => {
        if (filter === "Type") {
            setCurrentShips(allShips);
            return;
        }
        const filtered = allShips.filter(
            (current: Ship) =>
                current.type.toLowerCase() === filter.toLowerCase()
        );
        setCurrentShips(filtered);
    };

    return (
        <shipsContext.Provider
            value={{
                ships: currentShips,
                filterShips: setFilter,
                scrollPage: getMoreShips,
                pageNumber: page,
            }}
        >
            {children}
        </shipsContext.Provider>
    );
};

export default ShipsProvider;
